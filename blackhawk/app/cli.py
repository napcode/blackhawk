import os
from piccolo.main import main as piccolo_main


def main():
    os.environ.setdefault("PICCOLO_CONF", "app.piccolo_conf")
    piccolo_main()


if __name__ == "__main__":
    main()
