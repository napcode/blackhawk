from starlette.config import Config
from starlette.datastructures import CommaSeparatedStrings, Secret


config = Config(".env")

DEBUG = config("DEBUG", cast=bool, default=False)
DEFAULT_STATIC_DIR = "app/static" if DEBUG else "static"
STATIC_DIR = config("STATIC_DIR", default=DEFAULT_STATIC_DIR)
CORS_ALLOW_ORIGINS = config(
    "CORS_ALLOW_ORIGINS", cast=CommaSeparatedStrings, default=["*"]
)
LOG_LEVEL = config("LOG_LEVEL", default="NOTSET")
DB_NAME = config("DB_NAME", default="blackhawk")
DB_USER = config("DB_USER", default="postgres")
DB_PASS = config("DB_PASS", default="postgres")
DB_HOST = config("DB_HOST", default="localhost")
DB_PORT = config("DB_PORT", default="5432")
PROJECT_ID = config("PROJECT_ID", cast=int, default=1)
# SECRET_KEY = config('SECRET_KEY', cast=Secret)
# DATABASE_URL = config('DATABASE_URL')
