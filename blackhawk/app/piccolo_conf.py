from piccolo.engine.postgres import PostgresEngine
from piccolo.conf.apps import AppRegistry

from app import settings

VERSION = "0.1.3"

DB = PostgresEngine(
    config={
        "database": settings.DB_NAME,
        "user": settings.DB_USER,
        "password": settings.DB_PASS,
        "host": settings.DB_HOST,
        "port": settings.DB_PORT,
    }
)

APP_REGISTRY = AppRegistry(
    apps=[
        "piccolo.apps.user.piccolo_app",
        "core.piccolo_app",
        "storage.piccolo_app",
        "hub.piccolo_app",
        "events.piccolo_app",
    ]
)
