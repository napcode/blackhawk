from piccolo_conf import *  # noqa


DB = PostgresEngine(
    config={
        "database": "blackhawk_test",
        "user": "postgres",
        "password": "postgres",
        "host": "localhost",
        "port": 5432,
    }
)
