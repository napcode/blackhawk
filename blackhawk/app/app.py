import contextlib

import logging
from piccolo.engine import engine_finder
from starlette import status
from starlette.requests import Request
from starlette.routing import Route, Mount
from starlette.responses import JSONResponse
from starlette.applications import Starlette
from starlette.staticfiles import StaticFiles
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.middleware.authentication import AuthenticationMiddleware


from app import settings
from core.exceptions import AppException
from core.authentication import TokenAuthBackend
from dashboard.endpoints import DashboardPageEndpoint
from core.routes import routes as core_routes
from hub.routes import routes as hub_routes
from storage.routes import routes as storage_routes
from events.routes import routes as events_routes
from dashboard.routes import routes as dashboard_routes

logging.basicConfig(level=getattr(logging, settings.LOG_LEVEL.upper()))

middleware = [
    Middleware(CORSMiddleware, allow_origins=settings.CORS_ALLOW_ORIGINS),
    Middleware(AuthenticationMiddleware, backend=TokenAuthBackend()),
]


def handle_app_exception(request: Request, e: AppException):
    return JSONResponse(status_code=status.HTTP_400_BAD_REQUEST, content=e.data)


async def open_database_connection_pool():
    try:
        engine = engine_finder()
        await engine.start_connection_pool()
    except Exception:
        print("Unable to connect to the database")


async def close_database_connection_pool():
    try:
        engine = engine_finder()
        await engine.close_connection_pool()
    except Exception:
        print("Unable to connect to the database")


@contextlib.asynccontextmanager
async def lifespan(app):
    await open_database_connection_pool()
    yield
    await close_database_connection_pool()


app = Starlette(
    routes=[
        Route("/", DashboardPageEndpoint),
        Mount("/core", routes=core_routes),
        Mount("/hub", routes=hub_routes),
        Mount("/events", routes=events_routes),
        Mount("/storage", routes=storage_routes),
        Mount("/dashboard", routes=dashboard_routes),
        Mount("/static", StaticFiles(directory=settings.STATIC_DIR)),
    ],
    middleware=middleware,
    exception_handlers={AppException: handle_app_exception},
    lifespan=lifespan,
    debug=settings.DEBUG,
)
