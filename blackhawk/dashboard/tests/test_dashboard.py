from starlette import status

from core.tests.common import ComplexTestCase
from events.tables import Event
from events.enums import EventKind


class TestDashboardEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.project = self.create_project(self.user)
        self.project2 = self.create_project(self.user, name="project2")
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/dashboard/"
        Event(name="Test", project=self.project).save().run_sync()
        Event(
            name="Test2", kind=EventKind.ERROR, project=self.project
        ).save().run_sync()
        Event(name="Test", project=self.project2).save().run_sync()
        Event(
            name="Test2", kind=EventKind.DEBUG, project=self.project2
        ).save().run_sync()

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.get(self.url)
        assert response.status_code == status.HTTP_303_SEE_OTHER

    def test_valid(self):
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK


class TestCreateStorageHxEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.project = self.create_project(self.user)
        self.project2 = self.create_project(self.user, name="project2")
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/dashboard/hx-projects"
        Event(name="Test", project=self.project).save().run_sync()
        Event(
            name="Test2", kind=EventKind.ERROR, project=self.project
        ).save().run_sync()
        Event(name="Test", project=self.project2).save().run_sync()
        Event(
            name="Test2", kind=EventKind.DEBUG, project=self.project2
        ).save().run_sync()

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.post(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_empty_data(self):
        response = self.client.post(self.url, data={})
        assert response.status_code == status.HTTP_200_OK

    def test_valid_data(self):
        data = {"range": 3}
        response = self.client.post(self.url, data=data)
        assert response.status_code == status.HTTP_200_OK
