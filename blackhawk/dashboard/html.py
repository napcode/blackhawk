import htpy as html


def time_range_form(selected: str = "30") -> html.Element:
    return html.form({"hx-post": "/dashboard/hx-projects", "hx-target": "#projects"})[
        html.fieldset(role="group")[
            html.select(name="range", required=True)[
                html.option(value="3", selected=(selected == "3"))["Last 3 days"],
                html.option(value="7", selected=(selected == "7"))["Last 7 days"],
                html.option(value="30", selected=(selected == "30"))["Last 30 days"],
                html.option(value="90", selected=(selected == "90"))["Last 90 days"],
                html.option(value="all", selected=(selected == "all"))["All"],
            ],
            html.button(type="submit")["show"],
        ]
    ]


def projects_cards(projects: list[dict]) -> html.Element:
    def card(project: dict) -> html.Element:
        return html.article(".m-4", style="width: 350px; min-height: 200px")[
            html.header(".t-center")[html.b(".size-5")[project["name"]],],
            html.div(".f-col.f-jc-space-between")[
                html.div(".f-row.f-jc-space-around")[
                    html.div(".f-col")[
                        html.span["Sessions"],
                        html.span(".f-as-center")[project["sessions"]],
                    ],
                    html.div(".f-col")[
                        html.span["Storages"],
                        html.span(".f-as-center")[project["storages"]],
                    ],
                    html.div(".f-col")[
                        html.span["Rooms"], html.span(".f-as-center")[project["rooms"]]
                    ],
                ],
                html.hr,
                html.div[
                    html.table[
                        html.thead[html.tr[html.th["events"], html.th["count"]],],
                        [
                            html.tr[html.th[kind], html.th[count]]
                            for kind, count in project["events"].items()
                        ],
                    ],
                ],
            ],
            html.footer(".f-row.f-jc-center")[
                html.a(href="/events/list/{}".format(project["key"]))[
                    html.button["Details"]
                ]
            ],
        ]

    return html.div[
        html.div(".f-row.f-jc-space-around.f-wrap")[[card(p) for p in (projects)]]
    ]
