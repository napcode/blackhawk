import logging
import htpy as html
import datetime
from typing import Tuple


from starlette.endpoints import HTTPEndpoint
from starlette.requests import Request
from starlette.responses import HTMLResponse
from starlette.authentication import requires
from piccolo.query.functions.aggregate import Count

from core.html import page, nav
from core.tables import Project
from events.tables import Event
from storage.tables import Storage
from hub.services import Hub, Room
from dashboard.html import projects_cards, time_range_form

logger = logging.getLogger(__name__)


class ProjectsStatsMixin:
    def _prepare_data(
        self,
        projects: list[dict],
        events_count: list[dict],
        sessions_count: list[dict],
        storages_count: list[dict],
        rooms: list[Room],
    ) -> list[dict]:
        projects_map = {}
        for p in projects:
            p["events"] = {}
            p["sessions"] = 0
            p["storages"] = 0
            p["rooms"] = 0
            projects_map[p["id"]] = p
        for row in events_count:
            p = projects_map[row["project"]]
            p["events"][row["kind"]] = row["count"]
        for row in sessions_count:
            p = projects_map[row["project"]]
            p["sessions"] = row["count"]
        for row in storages_count:
            p = projects_map[row["project"]]
            p["storages"] = row["count"]
        for room in rooms:
            projects_map[room.project]["rooms"] += 1

        data = sorted(projects, key=lambda x: x["sessions"], reverse=True)

        return data

    async def _projects_stats(
        self, request: Request, time_range: str = "30"
    ) -> list[dict]:
        query = Project.select(Project.id, Project.key, Project.name)
        if not request.user.admin:
            query.where(Project.owner == request.user)
        projects = await query

        query = (
            Event.select(Event.project, Event.kind, Count(Event.kind, alias="count"))
            .where(Event.project.is_in([p["id"] for p in projects]))
            .group_by(Event.project, Event.kind)
            .order_by(Event.project, Event.kind)
        )
        if time_range in ["3", "7", "30", "90"]:
            now = datetime.datetime.now()
            days_ago = now - datetime.timedelta(days=int(time_range))
            query.where(Event.time > days_ago)
        events_count = await query

        query = (
            Event.select(Event.project, Count(distinct=[Event.session], alias="count"))
            .where(Event.session != "")
            .where(Event.project.is_in([p["id"] for p in projects]))
            .group_by(Event.project)
            .order_by(Event.project)
        )
        if time_range in ["3", "7", "30", "90"]:
            now = datetime.datetime.now()
            days_ago = now - datetime.timedelta(days=int(time_range))
            query.where(Event.time > days_ago)
        sessions_count = await query

        storages_count = await (
            Storage.select(Storage.project, Count(Storage.project))
            .group_by(Storage.project)
            .order_by(Storage.project)
        )

        rooms = list(Hub.rooms.values())

        return self._prepare_data(
            projects, events_count, sessions_count, storages_count, rooms
        )


class DashboardPageEndpoint(ProjectsStatsMixin, HTTPEndpoint):
    def _html(self, data: dict) -> html.Element:
        body = [
            nav(data["user"].admin),
            html.main(".container.f-col")[
                time_range_form(data["time_range"]),
                html.div("#projects")[projects_cards(data["projects"])],
            ],
        ]

        return page(title="Dashboard", body=body)

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        query = Storage.count()
        if not request.user.admin:
            query.where(Storage.owner == request.user)
        storages = await query

        time_range = "30"
        data = {
            "projects": await self._projects_stats(request, time_range),
            "rooms": len(Hub.rooms),
            "storages": storages,
            "time_range": time_range,
            "user": request.user,
        }

        return HTMLResponse(self._html(data))


class ProjectsCardsHxEndpoint(ProjectsStatsMixin, HTTPEndpoint):
    def _html(self, data: dict) -> html.Element:
        return projects_cards(data["projects"])

    @requires(["authenticated"])
    async def post(self, request: Request):
        time_range = "30"
        async with request.form() as form:
            time_range = str(form.get("range", "30"))

        projects = await self._projects_stats(request, time_range)

        return HTMLResponse(projects_cards(projects))
