from starlette.routing import Route

from . import endpoints

routes = [
    Route("/", endpoints.DashboardPageEndpoint),
    Route("/hx-projects", endpoints.ProjectsCardsHxEndpoint),
]
