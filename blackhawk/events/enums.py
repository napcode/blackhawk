from enum import StrEnum

class EventKind(StrEnum):
    DEBUG = 'debug'
    ERROR = 'error'
    INFO = 'info'
    ANALYTICS = 'analytics'

