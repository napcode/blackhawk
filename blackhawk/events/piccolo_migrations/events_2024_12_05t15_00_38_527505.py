from piccolo.apps.migrations.auto.migration_manager import MigrationManager
from piccolo.columns.column_types import JSON
from piccolo.columns.column_types import Text


ID = "2024-12-05T15:00:38:527505"
VERSION = "1.22.0"
DESCRIPTION = ""


async def forwards():
    manager = MigrationManager(
        migration_id=ID, app_name="events", description=DESCRIPTION
    )

    manager.alter_column(
        table_class_name="Event",
        tablename="event",
        column_name="data",
        db_column_name="data",
        params={"null": False},
        old_params={"null": True},
        column_class=JSON,
        old_column_class=JSON,
        schema=None,
    )

    manager.alter_column(
        table_class_name="Event",
        tablename="event",
        column_name="session",
        db_column_name="session",
        params={"null": False},
        old_params={"null": True},
        column_class=Text,
        old_column_class=Text,
        schema=None,
    )

    manager.alter_column(
        table_class_name="Event",
        tablename="event",
        column_name="identifier",
        db_column_name="identifier",
        params={"null": False},
        old_params={"null": True},
        column_class=Text,
        old_column_class=Text,
        schema=None,
    )

    manager.alter_column(
        table_class_name="Event",
        tablename="event",
        column_name="location",
        db_column_name="location",
        params={"null": False},
        old_params={"null": True},
        column_class=Text,
        old_column_class=Text,
        schema=None,
    )

    return manager
