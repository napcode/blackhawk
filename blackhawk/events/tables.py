import datetime
import hashlib

from piccolo.table import Table
from piccolo.columns import Text, ForeignKey, Timestamptz, JSON, Array
from piccolo.utils.pydantic import create_pydantic_model

from core.tables import Project
from events.enums import EventKind


def now() -> datetime.datetime:
    return datetime.datetime.now(tz=datetime.timezone.utc)


class Event(Table):
    name = Text()
    time = Timestamptz()
    kind = Text(choices=EventKind, default=EventKind.INFO)
    data = JSON()
    session = Text()
    location = Text()
    identifier = Text()
    tags = Array(base_column=Text(), default=[])
    project = ForeignKey(references=Project)

    @staticmethod
    def session_hash(ip: str, user_agent: str, project_key: str) -> str:
        session_hash = hashlib.sha256()
        session_hash.update(ip.encode("utf-8"))
        session_hash.update(user_agent.encode("utf-8"))
        session_hash.update(project_key.encode("utf-8"))
        session_hash.update(str(now().date().isoformat()).encode("utf-8"))

        return session_hash.hexdigest()
