from starlette.routing import Route

from events import endpoints

routes = [
    Route("/", endpoints.EventsPageEndpoint),
    Route("/capture/{project}", endpoints.CaptureEventApiEndpoint),
    Route("/pixel/{project}", endpoints.PixelEndpoint),
    Route("/list/{project}", endpoints.ProjectEventsPageEndpoint),
    Route("/detail/{event}", endpoints.EventDetailPageEndpoint),
    Route("/hx-events", endpoints.EventsHxEndpoint),
    Route("/hx-project-events/{project}", endpoints.ProjectEventsHxEndpoint),
]
