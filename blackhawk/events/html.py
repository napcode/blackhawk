import htpy as html


def time_range_form(selected: str, project: int = -1) -> html.Element:
    url = "/events/hx-events"
    if project != -1:
        url = f"/events/hx-project-events/{project}"

    return html.form({"hx-post": url, "hx-target": "#events"})[
        html.fieldset(role="group")[
            html.select(name="range", required=True)[
                html.option(value="3", selected=(selected == "3"))["Last 3 days"],
                html.option(value="7", selected=(selected == "7"))["Last 7 days"],
                html.option(value="30", selected=(selected == "30"))["Last 30 days"],
                html.option(value="90", selected=(selected == "90"))["Last 90 days"],
                html.option(value="all", selected=(selected == "all"))["All"],
            ],
            html.button(type="submit")["show"],
        ]
    ]


def events_table(events: list[dict], show_project: bool = False) -> html.Element:
    return html.div(id="events", style="width: 100%; overflow-y: auto;")[
        html.table(".striped")[
            html.thead[
                html.tr[
                    show_project and html.th["project"],
                    html.th["kind"],
                    html.th["name"],
                    html.th["time"],
                    html.th["session"],
                    html.th["data"],
                    html.th["more"],
                ]
            ],
            [event_row(e, show_project) for e in events],
        ]
    ]


def event_row(event: dict, show_project: bool = False) -> html.Element:
    return html.tr[
        show_project and html.td[event["project.name"]],
        html.td[event["kind"]],
        html.td[event["name"]],
        html.td[event["time"].strftime("%Y-%m-%d %H:%M")],
        html.td[html.small[event["session"][:12]]],
        html.td[html.small[event["data"]]],
        html.td[html.a(href="/events/detail/{}".format(event["id"]))["-->"]],
    ]
