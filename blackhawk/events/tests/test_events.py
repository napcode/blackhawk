import uuid
from starlette import status
from starlette.testclient import TestClient


from core.tests.common import ComplexTestCase
from events.tables import Event

from app.app import app


class TestCaptureEventApiEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.project = self.create_project(self.user)
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/events/capture/{}"

    def test_unauthorized(self):
        url = self.url.format(self.project.key)
        client = TestClient(app)
        response = client.post(url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_empty_json(self):
        url = self.url.format(self.project.key)
        response = self.client.post(url, json={})
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_not_existing_project(self):
        url = self.url.format(uuid.uuid4())
        response = self.client.post(url, json={})
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_invalid_project(self):
        url = self.url.format("abc")
        response = self.client.post(url, json={})
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_valid_data(self):
        url = self.url.format(self.project.key)
        response = self.client.post(url, json={"name": "test", "kind": "debug"})
        assert response.status_code == status.HTTP_200_OK
        result = Event.select(Event.name).first().run_sync()
        assert result is not None
        assert result["name"] == "test"

    def test_invalid_data(self):
        url = self.url.format(self.project.key)
        response = self.client.post(url, json={"name": 1, "kind": ["abc"]})
        assert response.status_code == status.HTTP_400_BAD_REQUEST


class TestPixelEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.project = self.create_project(self.user)
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/events/pixel/{}"

    def test_not_existing_project(self):
        url = self.url.format(uuid.uuid4())
        response = self.client.get(url)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_invalid_project(self):
        url = self.url.format("abc")
        response = self.client.get(url)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_valid(self):
        url = self.url.format(self.project.key)
        response = self.client.get(url)
        assert response.status_code == status.HTTP_200_OK
        result = Event.select(Event.name).first().run_sync()
        assert result is not None
        assert result["name"] == "pixel"


class TestEventsPageEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.project = self.create_project(self.user)
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/events/"
        Event(name="Test", project=self.project).save().run_sync()

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.get(self.url)
        assert response.status_code == status.HTTP_303_SEE_OTHER

    def test_valid(self):
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK


class TestProjectEventsPageEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.project = self.create_project(self.user)
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = f"/events/list/{self.project.key}"
        Event(name="Test", project=self.project).save().run_sync()

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.get(self.url)
        assert response.status_code == status.HTTP_303_SEE_OTHER

    def test_valid(self):
        response = self.client.get(self.url)
        assert response.status_code == status.HTTP_200_OK


class TestEventDetailPageEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.project = self.create_project(self.user)
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/events/detail/{}"
        Event(name="Test", project=self.project).save().run_sync()

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.get(self.url)
        assert response.status_code == status.HTTP_303_SEE_OTHER

    def test_valid(self):
        event = Event.select(Event.id).first().run_sync()
        response = self.client.get(self.url.format(event["id"]))
        assert response.status_code == status.HTTP_200_OK


class TestEventsHxEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.project = self.create_project(self.user)
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/events/hx-events"
        Event(name="Test", project=self.project).save().run_sync()

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.post(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_valid_no_data(self):
        response = self.client.post(self.url)
        assert response.status_code == status.HTTP_200_OK

    def test_valid(self):
        response = self.client.post(self.url, data={"time_range": "7"})
        assert response.status_code == status.HTTP_200_OK


class ProjectEventsHxEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.project = self.create_project(self.user)
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = f"/events/hx-project-events/{self.project.key}"
        Event(name="Test", project=self.project).save().run_sync()

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.post(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_valid_no_data(self):
        response = self.client.post(self.url)
        assert response.status_code == status.HTTP_200_OK

    def test_valid(self):
        response = self.client.post(self.url, data={"time_range": "7"})
        assert response.status_code == status.HTTP_200_OK
