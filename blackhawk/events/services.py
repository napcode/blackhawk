import logging

from events.tables import Event
from events.enums import EventKind
from app.settings import PROJECT_ID
from starlette.requests import Request


logger = logging.getLogger(__name__)


class InternalEvent:
    @staticmethod
    async def capture(
        msg: str,
        kind: EventKind,
        location: str,
        data: dict = {},
        tags: list[str] = [],
        request: Request | None = None,
    ):
        if not PROJECT_ID:
            return

        session = ""
        if request:
            ip = str(request.client.host) if request.client else ""
            user_agent = request.headers.get("user-agent", "Unknown").strip()
            session = Event.session_hash(ip=ip, user_agent=user_agent, project_key="1")

        await Event(
            name=msg,
            kind=kind,
            project=PROJECT_ID,
            location=location,
            data=data,
            session=session,
            tags=tags,
        ).save()
