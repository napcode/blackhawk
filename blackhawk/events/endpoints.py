import datetime
import logging
import base64
import uuid
import htpy as html

from typing import Any, List, Dict, Tuple

from starlette.endpoints import HTTPEndpoint, Response
from starlette.requests import Request
from starlette.responses import JSONResponse, HTMLResponse
from starlette.authentication import requires
from starlette.exceptions import HTTPException

from pydantic_core._pydantic_core import ValidationError
from pydantic import BaseModel, Field

from core.endpoints import ApiEndpoint
from core.exceptions import AppException
from core.html import page, nav
from core.tables import Project

from events.html import events_table, time_range_form
from events.tables import Event
from events.enums import EventKind

logger = logging.getLogger(__name__)


class ValidateProjectMixin:
    async def _validate_project(
        self, request: Request, check_user: bool = True
    ) -> Tuple[int, str]:
        key = request.path_params.get("project")
        try:
            uuid.UUID(key)
        except ValueError:
            raise AppException("Invalid Project key")

        select = Project.select(Project.id).where(Project.key == key)

        if check_user and not request.user.admin:
            select.where(Project.owner == request.user.id)

        project = await select.first()

        if not project:
            raise AppException("Project not found")

        return project["id"], key


class EventsMixin:
    async def _get_all_events(
        self, request: Request, time_range: str = "all"
    ) -> list[dict]:
        query = Event.select(
            Event.all_columns(),
            Event.project.name,
            Event.project.owner,
        ).order_by(Event.time, ascending=False)
        if not request.user.admin:
            query.where(Event.project.owner == request.user)

        if time_range != "all":
            date_from = datetime.datetime.now() - datetime.timedelta(
                days=int(time_range)
            )
            date_from.replace(hour=0, minute=0, second=0, microsecond=0)
            query.where(Event.time > date_from)

        events = await query

        return events

    async def _get_project_events(
        self, project: int, time_range: str = "all"
    ) -> list[dict]:
        query = (
            Event.select(Event.all_columns())
            .order_by(Event.time, ascending=False)
            .where(Event.project == project)
        )
        if time_range != "all":
            date_from = datetime.datetime.now() - datetime.timedelta(
                days=int(time_range)
            )
            date_from.replace(hour=0, minute=0, second=0, microsecond=0)
            query.where(Event.time > date_from)

        events = await query

        return events

    async def _get_time_range_from_form(self, request: Request) -> str:
        time_range = "30"
        async with request.form() as form:
            time_range = str(form.get("range", "30"))

        if time_range not in ["3", "7", "30", "90", "all"]:
            time_range = "30"

        return time_range


class CaptureEventApiEndpoint(ValidateProjectMixin, ApiEndpoint):
    class InputModel(BaseModel):
        name: str
        kind: EventKind = EventKind.INFO
        data: Dict[str, Any] = Field(default_factory=dict)
        identifier: str = ""
        session: str = ""
        tags: List[str] = []

    @requires(["authenticated"])
    async def post(self, request: Request):
        data = await self._get_json_body(request)
        data["project"], project_key = await self._validate_project(request)

        validated_data = self._validate_data(request, data)
        ip = str(request.client.host)
        user_agent = request.headers.get("User-Agent", "Unknown")
        validated_data["session"] = Event.session_hash(
            ip=ip, user_agent=user_agent, project_key=project_key
        )

        await Event(**validated_data).save()

        return JSONResponse(content={})

    def _validate_data(self, request: Request, data: dict) -> dict:
        try:
            model = self.InputModel(**data)
        except ValidationError as e:
            raise AppException(data={"errors": str(e.errors())})

        return model.model_dump()


class PixelEndpoint(ValidateProjectMixin, HTTPEndpoint):
    async def get(self, request: Request):
        project_id, project_key = await self._validate_project(
            request, check_user=False
        )

        ip = str(request.client.host)
        user_agent = request.headers.get("user-agent", "Unknown").strip()
        location = request.headers.get("referer", "/").strip()
        await Event(
            name="pixel",
            session=Event.session_hash(
                ip=ip, user_agent=user_agent, project_key=project_key
            ),
            kind=EventKind.ANALYTICS,
            project=project_id,
            location=location,
        ).save()

        pixel = base64.b64decode(
            "R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="
        )

        response = Response(pixel, media_type="image/gif")
        response.headers.update(
            {
                "Cache-Control": "no-cache, no-store, must-revalidate",
                "Access-Control-Allow-Origin": "*",
            }
        )

        return response


class ProjectEventsPageEndpoint(ValidateProjectMixin, EventsMixin, HTTPEndpoint):
    def _html(self, request: Request, data: dict) -> html.Element:
        body = [
            nav(request.user.admin),
            html.main(".container.f-col.f-ai-center")[
                time_range_form(data["time_range"], data["project"]),
                events_table(data["events"]),
            ],
        ]

        return page(title="Events", body=body)

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        project, project_key = await self._validate_project(request)
        events = await self._get_project_events(project)
        data = {"events": events, "project": project_key, "time_range": "30"}

        return HTMLResponse(self._html(request, data))


class EventsPageEndpoint(EventsMixin, HTTPEndpoint):
    def _html(self, request: Request, data: dict) -> html.Element:
        body = [
            nav(request.user.admin),
            html.main(".container.f-col.f-ai-center")[
                time_range_form(data["time_range"]),
                events_table(data["events"], show_project=True),
            ],
        ]

        return page(title="Events", body=body)

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        events = await self._get_all_events(request)
        data = {"events": events, "time_range": "30"}

        return HTMLResponse(self._html(request, data))


class EventDetailPageEndpoint(HTTPEndpoint):
    def _html(self, request: Request, data: dict) -> html.Element:
        body = [
            nav(request.user.admin),
            html.main(".container")[
                html.table[
                    html.tr[html.th, html.th],
                    html.tr[html.td["Kind:"], html.td[data["kind"]]],
                    html.tr[html.td["Name:"], html.td[data["name"]]],
                    html.tr[html.td["Project:"], html.td[data["project.name"]]],
                    html.tr[
                        html.td["Time:"],
                        html.td[data["time"].strftime("%Y-%m-%d %H:%M")],
                    ],
                    html.tr[html.td["location:"], html.td[data["location"]]],
                    html.tr[html.td["session:"], html.td[data["session"]]],
                    html.tr[html.td["identifier:"], html.td[data["identifier"]]],
                    html.tr[html.td["data:"], html.td[data["data"]]],
                    html.tr[html.td["tags:"], html.td[data["tags"]]],
                ]
            ],
        ]

        return page(title="Event", body=body)

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        event_id = self._get_event_id(request)

        query = Event.select(
            Event.all_columns(),
            Event.project.name,
            Event.project.owner,
        ).where(Event.id == event_id)

        if not request.user.admin:
            query.where(Event.project.owner == request.user)

        data = await query.first()

        if data is None:
            raise HTTPException(404, "Event not found")

        return HTMLResponse(self._html(request, data))

    def _get_event_id(self, request: Request) -> int:
        try:
            event_id = int(request.path_params.get("event"))
        except:
            raise AppException("Invalid event id")

        return event_id


class EventsHxEndpoint(EventsMixin, HTTPEndpoint):
    @requires(["authenticated"])
    async def post(self, request: Request):
        time_range = await self._get_time_range_from_form(request)
        events = await self._get_all_events(request, time_range)
        data = {"events": events}

        return HTMLResponse(events_table(data["events"], show_project=True))


class ProjectEventsHxEndpoint(ValidateProjectMixin, EventsMixin, HTTPEndpoint):
    @requires(["authenticated"])
    async def post(self, request: Request):
        time_range = await self._get_time_range_from_form(request)
        project, _ = await self._validate_project(request)
        events = await self._get_project_events(project, time_range)
        data = {"events": events}

        return HTMLResponse(events_table(data["events"]))
