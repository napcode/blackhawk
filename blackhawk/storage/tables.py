import uuid
from pydantic import BaseModel, BeforeValidator
from typing_extensions import Annotated

from piccolo.table import Table
from piccolo.columns import Text, JSONB, Array, ForeignKey
from piccolo.apps.user.tables import BaseUser

from core.tables import Project


class Storage(Table):
    key = Text(unique=True)
    name = Text()
    db = JSONB(default={})
    schema = JSONB(null=True)
    readonly_fields = Array(base_column=Text(), default=[])
    owner = ForeignKey(references=BaseUser)
    project = ForeignKey(references=Project)


class StorageModel(BaseModel):
    key: Annotated[str, BeforeValidator(lambda v: v or str(uuid.uuid4()))]
    name: str
    db: dict
    schema: dict | None = None
    readonly_fields: list[str] = []
    owner: int
    project: int
