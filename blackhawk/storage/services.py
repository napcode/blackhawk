import logging
from typing import Any, Union, Callable
from dataclasses import dataclass, field
from core.exceptions import AppException
from jsonschema import Draft202012Validator as Validator

logger = logging.getLogger(__name__)


@dataclass
class JsonDb:
    COMMANDS = ("set", "get", "add", "remove")
    db: dict
    schema: dict | None = None
    readonly_fields: list[str] = field(default_factory=list)

    def run_command(self, command: dict) -> dict:
        response = {}
        if "commands" in command:
            for idx, cmd in enumerate(command["commands"]):
                try:
                    response[idx] = self._run(cmd)
                except AppException as e:
                    e.data = {idx: e.data}
                    logger.debug(f"Command failed: {command}, {e.data}")
                    raise e
        else:
            response = self._run(command)

        logger.debug(f"Command succeded: {command}, {response}")

        return response

    def _run(self, command: dict) -> dict:
        command_type = command.get("type")
        if command_type not in self.COMMANDS:
            raise AppException({"error": "Invalid command"})

        if command_type == "get":
            return getattr(self, f"_{command_type}")(command)

        self._check_for_readonly_fields(command)
        response = getattr(self, f"_{command_type}")(command)
        self.validate()

        return response

    def _get(self, command: dict) -> dict:
        if "field" in command:
            name, value = self._get_field(command)
            return {name: value}
        elif "fields" in command:
            fields = [self._get_field(field) for field in command["fields"]]
            return {name: value for (name, value) in fields}

        return self.db

    def _get_field(self, field: Union[str, dict]) -> tuple[str | None, Any | None]:
        name = None
        if isinstance(field, str):
            name = field
        if isinstance(field, dict):
            name = field.get("field")
            if isinstance(self.db.get(name), list) and "where" in field:
                filter_func = self._get_filter_func(field["where"])

                return name, [value for value in filter(filter_func, self.db[name])]

        return name, self.db.get(name)

    def _set(self, command: dict) -> dict[str, str]:
        field = command["field"]
        if isinstance(self.db.get(field), list) and "where" in command:
            filter_func = self._get_filter_func(command["where"])
            index, _ = self._get_first_maching(self.db[field], filter_func)
            if index:
                self.db[field][index] = command.get("value")
        else:
            self.db[field] = command.get("value")

        return {"response": "ok"}

    def _add(self, command: dict) -> dict[str, str]:
        field = command["field"]
        value = command.get("value")
        max_length = command.get("max_length")
        if field not in self.db:
            raise AppException({"error": f"No field named '{field}'"})
        if not isinstance(self.db[field], list):
            raise AppException({"error": "Field is not a list"})

        self.db[field].append(value)

        if isinstance(max_length, int):
            while len(self.db[field]) > max_length:
                self.db[field].pop(0)

        return {"response": "ok"}

    def _remove(self, command: dict) -> dict[str, str]:
        if command["field"] not in self.db:
            return {"response": "ok"}

        field = command["field"]
        if isinstance(self.db[field], list) and "where" in command:
            filter_func = self._get_filter_func(command["where"])
            for item in filter(filter_func, self.db[field]):
                self.db[field].remove(item)
        else:
            del self.db[command["field"]]

        return {"response": "ok"}

    def _get_filter_func(self, where: dict) -> Callable[[Any], bool]:
        where_field = where.get("field")
        where_type = where.get("type")
        where_value = self._convert_where_value(where.get("value"), where_type)
        operator = where.get("operator", "=")

        def filter_func(item):
            field = item
            if isinstance(item, dict):
                field = item.get(where_field)
                if not field:
                    return False

            try:
                match operator:
                    case "=":
                        return field == where_value
                    case ">":
                        return field > where_value
                    case ">=":
                        return field >= where_value
                    case "<":
                        return field < where_value
                    case "<=":
                        return field <= where_value
            except TypeError:
                raise AppException({"error": "Invalid where value type"})

            return False

        return filter_func

    def _get_first_maching(
        self, collection: list[Any], filter_func: Callable[[Any], bool]
    ) -> tuple[int | None, Any]:
        for index, item in enumerate(collection):
            if filter_func(item):
                return index, item

        return None, None

    def _convert_where_value(
        self, where_value: Any, where_type: str | None
    ) -> Union[str, int, float]:
        try:
            if where_type == "int":
                return int(where_value)
            elif where_value == "float":
                return float(where_value)
        except ValueError:
            raise AppException({"error": "Where value does not match where type"})

        return where_value

    def _check_for_readonly_fields(self, command: dict):
        if "field" not in command:
            raise AppException({"error": "No field specified"})
        if command["field"] in self.readonly_fields:
            raise AppException({"error": f"Field '${command['field']}' is readonly"})

    def validate(self):
        if self.schema is None:
            return

        try:
            v = Validator(self.schema)
            errors = sorted(v.iter_errors(self.db), key=lambda e: e.path)
        except Exception:
            raise AppException({"error": "Schema error"})

        if errors:
            messages = [
                "{}: {}".format(self._as_index(e.path), e.message) for e in errors
            ]
            raise AppException({"error": "Validation failed", "details": messages})

    def _as_index(self, path: list) -> str:
        path = path or []
        return f"Value[{']['.join(repr(index) for index in path)}]"
