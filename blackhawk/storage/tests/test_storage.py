import pytest
from unittest import TestCase

from starlette import status


from core.exceptions import AppException
from core.tests.common import ComplexTestCase
from storage.tables import Storage
from storage.services import JsonDb


class TestJsonDb(TestCase):
    def setUp(self):
        self.data = {
            "color": "black",
            "number": 10,
            "list": [1, 2, 3],
            "dicts": [
                {"id": 1, "name": "a"},
                {"id": 2, "name": "b"},
                {"id": 3, "name": "c"},
            ],
        }
        self.schema = {
            "type": "object",
            "properties": {
                "color": {"type": "string"},
                "number": {"type": "number"},
                "list": {"type": "array", "item": {"type": "number"}},
                "dicts": {
                    "type": "array",
                    "itmes": {
                        "type": "object",
                        "properties": {
                            "id": {"type": "number"},
                            "name": {"type": "string"},
                        },
                    },
                },
            },
        }

    def test_invalid_command(self):
        db = JsonDb(db=dict(self.data))
        with pytest.raises(AppException):
            db.run_command({"type": "jump"})

    def test_get_no_field(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command({"type": "get"})

        assert response == self.data

    def test_get_one_field(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command({"type": "get", "field": "number"})

        assert response == {"number": self.data["number"]}

    def test_get_more_fields(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command({"type": "get", "fields": ["number", "color"]})

        assert response == {"color": self.data["color"], "number": self.data["number"]}

    def test_get_list_where(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command(
            {
                "type": "get",
                "field": "list",
                "where": {"type": "int", "value": 2, "operator": ">="},
            }
        )

        assert response == {"list": [2, 3]}

    def test_get_dicts_where(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command(
            {
                "type": "get",
                "field": "dicts",
                "where": {"field": "id", "type": "int", "value": 3, "operator": "<"},
            }
        )

        assert response == {"dicts": [{"id": 1, "name": "a"}, {"id": 2, "name": "b"}]}

    def test_set(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command({"type": "set", "field": "color", "value": "white"})

        assert response == {"response": "ok"}
        assert db.db["color"] == "white"

    def test_set_new_field(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command({"type": "set", "field": "new", "value": "value"})

        assert response == {"response": "ok"}
        assert db.db["new"] == "value"

    def test_set_where(self):
        db = JsonDb(db=dict(self.data))
        new_value = {"id": 2, "name": "d"}
        response = db.run_command(
            {
                "type": "set",
                "field": "dicts",
                "value": new_value,
                "where": {"field": "id", "value": 2},
            }
        )

        assert response == {"response": "ok"}
        assert db.db["dicts"][1] == new_value

    def test_remove(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command({"type": "remove", "field": "color"})

        assert response == {"response": "ok"}
        assert "color" not in db.db

    def test_remove_where(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command(
            {
                "type": "remove",
                "field": "dicts",
                "where": {"field": "id", "value": 2},
            }
        )

        assert response == {"response": "ok"}
        assert len(db.db["dicts"]) == 2

    def test_add(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command({"type": "add", "field": "list", "value": 4})

        assert response == {"response": "ok"}
        assert len(db.db["list"]) == 4

    def test_add_max_length(self):
        db = JsonDb(db=dict(self.data))
        response = db.run_command(
            {"type": "add", "field": "list", "value": 4, "max_length": 3}
        )

        assert response == {"response": "ok"}
        assert len(db.db["list"]) == 3
        assert db.db["list"][-1] == 4

    def test_schema_validation_success(self):
        db = JsonDb(db=dict(self.data), schema=self.schema)
        response = db.run_command({"type": "set", "field": "color", "value": "white"})

        assert response == {"response": "ok"}

    def test_schema_validation_fail(self):
        db = JsonDb(db=dict(self.data), schema=self.schema)
        with pytest.raises(AppException):
            db.run_command({"type": "set", "field": "color", "value": 1})


class TestStorageCommandApiEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/storage/cmd"
        storage = Storage(key="1", name="test", db={}, owner=self.user)
        storage.save().run_sync()

    def tearDown(self):
        pass

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.post(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_empty_json(self):
        response = self.client.post(self.url, json={})
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_not_existing_key(self):
        response = self.client.post(self.url, json={"key": "nil"})
        assert response.status_code == status.HTTP_404_NOT_FOUND

    def test_valid_command(self):
        response = self.client.post(
            self.url, json={"key": "1", "type": "set", "field": "number", "value": 1}
        )
        assert response.status_code == status.HTTP_200_OK
        result = (
            Storage.select(Storage.db)
            .where(Storage.key == "1")
            .output(load_json=True)
            .first()
            .run_sync()
        )
        assert "number" in result["db"]
        assert result["db"]["number"] == 1

    def test_invalid_command(self):
        response = self.client.post(
            self.url, json={"key": "1", "kind": "set", "column": "number", "value": 1}
        )
        assert response.status_code == status.HTTP_400_BAD_REQUEST


class TestUpdateStorageHxEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/storage/hx-update"
        storage = Storage(key="1", name="test", db={}, owner=self.user)
        storage.save().run_sync()

    def tearDown(self):
        self.drop_db_tables()

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.post(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_empty_data(self):
        response = self.client.post(self.url, data={})
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") != -1

    def test_valid_data_with_empty_db_and_scheme(self):
        data = {
            "key": "1",
            "name": "updated",
            "owner": self.user.id,
            "db": "{}",
            "schema": "{}",
            "readonly_fields": "[]",
            "project": 1,
        }
        response = self.client.post(self.url, data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") == -1

        storage = Storage.select(Storage.name).first().run_sync()
        assert storage["name"] == data["name"]

    def test_valid_data(self):
        data = {
            "key": "1",
            "name": "updated",
            "owner": self.user.id,
            "db": '{"a": 1, "b": "b"}',
            "schema": '{"type":"object", "properties": { "a":{"type":"integer"}, "b": {"type": "string"}}}',
            "readonly_fields": "[]",
            "project": 1,
        }
        response = self.client.post(self.url, data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") == -1

        storage = Storage.select(Storage.name).first().run_sync()
        assert storage["name"] == data["name"]

    def test_db_invalid_with_schema(self):
        data = {
            "key": "1",
            "name": "updated",
            "owner": self.user.id,
            "db": '{"a": "a", "b": 1}',
            "schema": '{"type":"object", "properties": { "a":{"type":"integer"}, "b": {"type": "string"}}}',
            "readonly_fields": "[]",
            "project": 1,
        }
        response = self.client.post(self.url, data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") != -1

        storage = Storage.select(Storage.name).first().run_sync()
        assert storage["name"] != data["name"]

    def test_parsing_error(self):
        data = {
            "key": "1",
            "name": "updated",
            "owner": "abc",
            "db": "{}",
            "schema": "{}",
            "readonly_fields": "abc",
        }
        response = self.client.post(self.url, data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") != -1


class TestCreateStorageHxEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        self.url = "/storage/hx-create"

    def tearDown(self):
        self.drop_db_tables()

    def test_unauthorized(self):
        client = self.create_unauthorized_client()
        response = client.post(self.url)
        assert response.status_code == status.HTTP_403_FORBIDDEN

    def test_empty_data(self):
        response = self.client.post(self.url, data={})
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") != -1

        count = Storage.count().run_sync()
        assert count == 0

    def test_valid_data_with_empty_db_and_scheme(self):
        data = {
            "name": "created",
            "owner": self.user.id,
            "db": "{}",
            "schema": "{}",
            "readonly_fields": "[]",
            "project": 1,
        }
        response = self.client.post(self.url, data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") == -1

        storage = Storage.select(Storage.name).first().run_sync()
        assert storage["name"] == data["name"]

    def test_valid_data(self):
        data = {
            "name": "created",
            "owner": self.user.id,
            "db": '{"a": 1, "b": "b"}',
            "schema": '{"type":"object", "properties": { "a":{"type":"integer"}, "b": {"type": "string"}}}',
            "readonly_fields": "[]",
            "project": 1,
        }
        response = self.client.post(self.url, data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") == -1

        storage = Storage.select(Storage.name).first().run_sync()
        assert storage["name"] == data["name"]

    def test_data_invalid_with_schema(self):
        data = {
            "name": "created",
            "owner": self.user.id,
            "db": '{"a": "a", "b": 1}',
            "schema": '{"type":"object", "properties": { "a":{"type":"integer"}, "b": {"type": "string"}}}',
            "readonly_fields": "[]",
            "project": 1,
        }
        response = self.client.post(self.url, data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") != -1

        count = Storage.count().run_sync()
        assert count == 0

    def test_parsing_error(self):
        data = {
            "name": "created",
            "owner": "abc",
            "db": "{}",
            "schema": "{}",
            "readonly_fields": "abc",
        }
        response = self.client.post(self.url, data=data)
        assert response.status_code == status.HTTP_200_OK
        assert response.text.find("form-msg") != -1

        count = Storage.count().run_sync()
        assert count == 0
