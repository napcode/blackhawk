from piccolo.apps.migrations.auto.migration_manager import MigrationManager
from piccolo.columns.column_types import JSON
from piccolo.columns.column_types import JSONB


ID = "2025-03-03T20:53:20:182471"
VERSION = "1.23.0"
DESCRIPTION = ""


async def forwards():
    manager = MigrationManager(
        migration_id=ID, app_name="storage", description=DESCRIPTION
    )

    manager.alter_column(
        table_class_name="Storage",
        tablename="storage",
        column_name="db",
        db_column_name="db",
        params={},
        old_params={},
        column_class=JSONB,
        old_column_class=JSON,
        schema=None,
    )

    manager.alter_column(
        table_class_name="Storage",
        tablename="storage",
        column_name="schema",
        db_column_name="schema",
        params={},
        old_params={},
        column_class=JSONB,
        old_column_class=JSON,
        schema=None,
    )

    return manager
