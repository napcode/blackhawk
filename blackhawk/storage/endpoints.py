import uuid
import json
import logging
import htpy as html

from starlette import status
from starlette.requests import Request
from starlette.endpoints import HTTPEndpoint
from starlette.exceptions import HTTPException
from starlette.responses import JSONResponse, HTMLResponse, Response
from starlette.authentication import requires

from pydantic_core._pydantic_core import ValidationError

from core.endpoints import ApiEndpoint
from core.exceptions import AppException
from core.html import page, nav
from events.services import InternalEvent, EventKind

from .services import JsonDb
from .tables import Storage, StorageModel
from .html import storage_form

logger = logging.getLogger(__name__)


class StorageCommandApiEndpoint(ApiEndpoint):
    @requires(["authenticated"])
    async def post(self, request):
        command = await self._get_json_body(request)
        key = command.get("key")
        if not key:
            raise AppException({"error": "No db key in command"})

        data = (
            await Storage.select(Storage.db, Storage.schema, Storage.readonly_fields)
            .where(Storage.key == key, Storage.owner == request.user.id)
            .output(load_json=True)
            .first()
        )

        if not data:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)

        db = JsonDb(**data)
        response = db.run_command(command)

        await Storage.update({Storage.db: db.db}).where(Storage.key == key)

        await InternalEvent.capture(
            msg="storage cmd api ",
            kind=EventKind.ANALYTICS,
            location="/storage/endpoints/StorageCommandApiEndpint",
            data={"user": request.user.id, "storage": key},
            tags=["api"],
            request=request,
        )

        return JSONResponse(content=response)


class StorageHomePageEndpoint(HTTPEndpoint):
    def _storage_row(self, storage: Storage) -> html.Element:
        return html.tr[
            html.td[html.small[storage["key"]]],
            html.td[storage["name"]],
            html.td[storage["owner.username"]],
            html.td[storage["project.name"]],
            html.td(".f-row.f-jc-space-evenly")[
                html.a(href=f"edit/{storage['key']}")["edit"]
            ],
        ]

    def _html(self, request: Request, data: dict) -> html.Element:
        body = [
            nav(request.user.admin),
            html.main(".container.f-col.f-ai-center")[
                html.h1["Storage"],
                html.table(".striped")[
                    html.thead[
                        html.tr()[
                            html.th["key"],
                            html.th["name"],
                            html.th["owner"],
                            html.th["project"],
                            html.th["actions"],
                        ]
                    ],
                    [self._storage_row(s) for s in data["storages"]],
                ],
                html.a(href="new")[html.button["Add new"]],
            ],
        ]
        return page(title="Storage", body=body)

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        select = Storage.select(
            Storage.key, Storage.name, Storage.owner.username, Storage.project.name
        )
        if not request.user.admin:
            select.where(Storage.owner == request.user.id)

        storages = await select
        data = {"storages": storages}

        return HTMLResponse(self._html(request, data))


class StorageEditPageEndpoint(HTTPEndpoint):
    def _html(self, request: Request, data: dict) -> html.Element:
        storage = data["storage"]
        body = [
            nav(request.user.admin),
            html.main(".container.f-col.f-ai-center")[
                html.h1[storage["name"]],
                html.article(".f-as-stretch")[
                    storage_form(storage), html.div("#form-msg")
                ],
            ],
        ]

        return page(title="Storage", body=body)

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        key = request.path_params.get("key")

        select = Storage.select(
            Storage.key,
            Storage.name,
            Storage.schema,
            Storage.db,
            Storage.readonly_fields,
            Storage.owner,
            Storage.project,
        ).where(Storage.key == key)
        if not request.user.admin:
            select.where(Storage.owner == request.user.id)
        storage = await select.first()

        storage["readonly_fields"] = json.dumps(storage["readonly_fields"])
        data = {"storage": storage}

        return HTMLResponse(self._html(request, data))


class StorageNewPageEndpoint(HTTPEndpoint):
    def _html(self, request: Request, data: dict) -> html.Element:
        storage = data["storage"]
        body = [
            nav(request.user.admin),
            html.main(".container.f-col.f-ai-center")[
                html.h1["New Storage"],
                html.article(".f-as-stretch")[
                    storage_form(storage), html.div(id="form-msg")
                ],
            ],
        ]

        return page(title="Storage", body=body)

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        data = {"storage": {}}

        return HTMLResponse(self._html(request, data))


class UpdateStorageHxEndpoint(HTTPEndpoint):
    def _html(self, data: dict) -> html.Element:
        return html.div("#form-msg.error")[data["msg"]]

    @requires(["authenticated"])
    async def post(self, request: Request):
        try:
            storage = await self._validate(request)
            await self._update(storage)
        except AppException as e:
            msg = e.data["error"]
            if "details" in e.data:
                msg += ": " + str(e.data["details"])
            return HTMLResponse(self._html({"msg": msg}))

        await InternalEvent.capture(
            msg="storage update",
            kind=EventKind.ANALYTICS,
            location="/storage/endpoints/UpdateStorageHxEndpoint",
            data={"user": request.user.id, "storage": storage.key},
            tags=["web"],
            request=request,
        )

        return Response(headers={"Hx-Location": "/storage/"})

    async def _parse_form(self, request: Request) -> dict:
        data = {}
        async with request.form() as form:
            data["key"] = form.get("key")
            data["name"] = form.get("name")
            try:
                data["owner"] = int(form.get("owner"))
            except Exception:
                raise AppException({"error": "Owner: parsing error"})

            try:
                data["project"] = int(form.get("project"))
            except Exception:
                raise AppException({"error": "Project: parsing error"})

            try:
                readonly_fields = form.get("readonly_fields") or "[]"
                data["readonly_fields"] = json.loads(readonly_fields)
            except Exception:
                raise AppException({"error": "Readonly fields: parsing error"})

            try:
                db = form.get("db") or "{}"
                data["db"] = json.loads(db)
            except Exception:
                raise AppException({"error": "DB: parsing error"})

            try:
                schema = form.get("schema")
                data["schema"] = json.loads(schema) if schema else None
            except Exception:
                raise AppException({"error": "Schema: parsing error"})

        return data

    async def _validate(self, request: Request) -> StorageModel:
        data = await self._parse_form(request)
        storage = self._validate_data(request, data)

        if not request.user.admin:
            await self._validate_ownership(request, storage)

        if storage.schema and storage.schema not in ("", "{}"):
            self._validate_db_with_schema(request, storage)

        return storage

    async def _validate_ownership(self, request: Request, storage: StorageModel):
        result = (
            await Storage.select(Storage.owner)
            .where(Storage.key == storage.key)
            .first()
        )
        if result["owner"] != request.user.id:
            raise AppException({"error": "Storage not found"})

    def _validate_data(self, request: Request, data: dict) -> StorageModel:
        try:
            storage = StorageModel(**data)
        except ValidationError as e:
            raise AppException(data={"error": str(e)})

        return storage

    def _validate_db_with_schema(self, request: Request, storage: StorageModel):
        JsonDb(storage.db, storage.schema).validate()

    async def _update(self, storage: StorageModel):
        try:
            await Storage.update(
                {
                    "name": storage.name,
                    "owner": storage.owner,
                    "readonly_fields": storage.readonly_fields,
                    "db": storage.db,
                    "schema": storage.schema,
                    "project": storage.project,
                }
            ).where(Storage.key == storage.key)
        except Exception as e:
            raise AppException(data={"error": str(e)})


class CreateStorageHxEndpoint(HTTPEndpoint):
    def _html(self, data: dict) -> html.Element:
        return html.div("#form-msg.error")[data["msg"]]

    @requires(["authenticated"])
    async def post(self, request: Request):
        try:
            storage = await self._validate(request)
            await self._create(storage)
        except AppException as e:
            msg = e.data["error"]
            if "details" in e.data:
                msg += ": " + str(e.data["details"])
            return HTMLResponse(self._html({"msg": msg}))

        return Response(headers={"Hx-Location": "/storage/"})

    async def _parse_form(self, request: Request) -> dict:
        data = {}
        async with request.form() as form:
            data["key"] = ""
            data["name"] = form.get("name")
            try:
                data["owner"] = int(form.get("owner"))
            except Exception:
                raise AppException({"error": "Owner: parsing error"})

            try:
                data["project"] = int(form.get("project"))
            except Exception:
                raise AppException({"error": "Project: parsing error"})

            try:
                readonly_fields = form.get("readonly_fields") or "[]"
                data["readonly_fields"] = json.loads(readonly_fields)
            except Exception:
                raise AppException({"error": "Readonly fields: parsing error"})

            try:
                db = form.get("db") or "{}"
                data["db"] = json.loads(db)
            except Exception:
                raise AppException({"error": "DB: parsing error"})

            try:
                schema = form.get("schema")
                data["schema"] = json.loads(schema) if schema else None
            except Exception:
                raise AppException({"error": "Schema: parsing error"})

        return data

    async def _validate(self, request: Request) -> StorageModel:
        data = await self._parse_form(request)
        storage = self._validate_data(request, data)

        if not request.user.admin:
            await self._validate_ownership(request, storage)

        if storage.schema and storage.schema not in ("", "{}"):
            self._validate_db_with_schema(request, storage)

        return storage

    async def _validate_ownership(self, request: Request, storage: StorageModel):
        if storage.owner != request.user.id:
            raise AppException({"error": "You have to be owner of storage"})

    def _validate_data(self, request: Request, data: dict) -> StorageModel:
        try:
            storage = StorageModel(**data)
        except ValidationError as e:
            raise AppException(data={"error": str(e)})

        return storage

    def _validate_db_with_schema(self, request: Request, storage: StorageModel):
        JsonDb(storage.db, storage.schema).validate()

    async def _create(self, storage: StorageModel):
        storage.key = str(uuid.uuid4())
        try:
            await Storage(**storage.model_dump()).save()
        except Exception as e:
            raise AppException(data={"error": str(e)})
