import htpy as html


def storage_form(storage: dict) -> html.Element:
    key = storage.get("key")
    hx_url = "/storage/hx-update" if key else "/storage/hx-create"

    return html.form({"hx-post": hx_url, "hx-target": "#form-msg"})[
        html.div(".grid")[
            html.fieldset[
                key
                and html.label[
                    "Key", html.input(name="key", value=str(key), readonly="")
                ],
                html.label[
                    "Name",
                    html.input(
                        name="name", value=storage.get("name", ""), required=True
                    ),
                ],
                html.label[
                    "Owner",
                    html.input(
                        name="owner", value=storage.get("owner", ""), required=True
                    ),
                ],
                html.label[
                    "Project",
                    html.input(
                        name="project", value=storage.get("project", ""), required=True
                    ),
                ],
                html.label[
                    "Readonly fields",
                    html.input(
                        name="readonly_fields",
                        value=storage.get("readonly_fields", ""),
                    ),
                ],
            ],
            html.fieldset[
                html.label[
                    "DB",
                    html.textarea(name="db", rows="7", required=True)[
                        storage.get("db", "{}")
                    ],
                ],
                html.label[
                    "Schema",
                    html.textarea(name="schema", rows="7")[storage.get("schema", "")],
                ],
            ],
        ],
        html.input(type="submit", value="save"),
        html.div("#form-msg"),
    ]
