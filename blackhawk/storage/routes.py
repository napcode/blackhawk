from starlette.routing import Route

from . import endpoints

routes = [
    Route("/", endpoints.StorageHomePageEndpoint),
    Route("/hx-update", endpoints.UpdateStorageHxEndpoint),
    Route("/hx-create", endpoints.CreateStorageHxEndpoint),
    Route("/cmd", endpoints.StorageCommandApiEndpoint),
    Route("/edit/{key}", endpoints.StorageEditPageEndpoint),
    Route("/new", endpoints.StorageNewPageEndpoint),
]
