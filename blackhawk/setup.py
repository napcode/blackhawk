import os
from setuptools import find_packages, setup

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name="blackhawk",
    version="0.1.3",
    author="napcode",
    author_email="nap@napcode.eu",
    packages=find_packages(),
    package_data={"app": ["static/*"]},
    include_package_data=True,
    zip_safe=False,
    entry_points={"console_scripts": {"blackhawk = app.cli:main"}},
)
