import uuid
import string
import logging
import random
from dataclasses import dataclass
from enum import StrEnum
from typing import Type


from starlette.websockets import WebSocket
from core.exceptions import AppException
from app.settings import PROJECT_ID

logger = logging.getLogger(__name__)


@dataclass
class Client:
    key: str
    websocket: WebSocket

    @classmethod
    def new(cls, websocket: WebSocket):
        return cls(key=str(uuid.uuid4()), websocket=websocket)


class Room:
    """Plain room broadcasts all messages to everyone"""

    name: str
    project: int

    clients: list[Client]
    spectators: list[Client]

    def __init__(self, name: str, project: int = PROJECT_ID):
        self.name = name
        self.project = project
        self.clients = []
        self.spectators = []

    @property
    def size(self) -> int:
        return len(self.clients)

    @property
    def is_empty(self) -> bool:
        return self.size == 0

    @property
    def is_dev(self) -> bool:
        return self.name[0] == "d"

    def add_client(self, websocket: WebSocket) -> str:
        client = Client.new(websocket)
        self.clients.append(client)

        return client.key

    def add_spectator(self, websocket: WebSocket) -> str:
        client = Client.new(websocket)
        self.spectators.append(client)

        return client.key

    def remove_client(self, websocket: WebSocket) -> str | None:
        clients_to_remove = [c for c in self.clients if c.websocket == websocket]
        self.clients = [c for c in self.clients if c.websocket != websocket]

        if len(clients_to_remove) > 0:
            return clients_to_remove[0].key

        return None

    def remove_spectator(self, websocket: WebSocket) -> str | None:
        clients_to_remove = [c for c in self.spectators if c.websocket == websocket]
        self.spectators = [c for c in self.spectators if c.websocket != websocket]

        if len(clients_to_remove) > 0:
            return clients_to_remove[0].key

        return None

    def get_client_key(self, websocket: WebSocket) -> str | None:
        clients = [c for c in self.clients if c.websocket == websocket]

        if len(clients) == 0:
            return None

        return clients[0].key

    async def send(self, sender_key: str, data: dict):
        message = {"sender": sender_key, "data": data}
        for client in self.clients:
            await client.websocket.send_json(message)

        for client in self.spectators:
            await client.websocket.send_json(message)

        logger.debug(f"Message sent: {self.name}, {sender_key}, {data}")

    async def direct_send(self, sender_key: str, reciever_key: str, data: dict):
        message = {"sender": sender_key, "data": data}
        for client in self.clients:
            if client.key == reciever_key:
                await client.websocket.send_json(message)

        for client in self.spectators:
            await client.websocket.send_json(message)

        logger.debug(
            f"Direct message sent: {self.name}, {sender_key}, {reciever_key}, {data}"
        )

    async def close_websocets(self):
        for client in self.clients:
            await client.websocket.close()


class MasterRoom(Room):
    """Master room sends all messages from first client to other clients and from other clients to first client"""

    def __init__(self, name: str, project: int = PROJECT_ID):
        super().__init__(name, project)

    async def send(self, sender_key: str, data: dict):
        if self.is_empty:
            return

        message = {"sender": sender_key, "data": data}
        if sender_key != self.clients[0].key:
            await self.clients[0].websocket.send_json(message)
        else:
            for client in self.clients[1:]:
                await client.websocket.send_json(message)

        for client in self.spectators:
            await client.websocket.send_json(message)

        logger.debug(f"Message sent: {self.name}, {sender_key}, {data}")


class RoomType(StrEnum):
    PLAIN = "plain"
    MASTER = "master"

    @classmethod
    def get_class(cls, type_name: str) -> Type[Room]:
        if type_name == cls.MASTER:
            return MasterRoom
        else:
            return Room

    @classmethod
    def all(cls) -> list[str]:
        return [cls.PLAIN, cls.MASTER]


class RoomDoesNotExist(Exception):
    pass


class Hub:
    ROOM_NAME_LENGTH = 3
    ROOM_NAME_CHARS = string.digits
    rooms: dict[str, Room] = {}

    @classmethod
    def get_room_name(cls) -> str:
        return "".join(random.choices(cls.ROOM_NAME_CHARS, k=cls.ROOM_NAME_LENGTH))

    @classmethod
    def clear(cls):
        cls.rooms = {}

    @classmethod
    def room_exists(cls, name: str) -> bool:
        return name in cls.rooms

    @classmethod
    def create_room(cls, room_type: str, project: int = PROJECT_ID) -> str:
        name = cls.get_room_name()
        while name in cls.rooms:
            name = cls.get_room_name()

        room_class = RoomType.get_class(room_type)
        cls.rooms[name] = room_class(name, project)

        logger.debug(f"Room created: {name}")

        return name

    @classmethod
    def create_room_if_not_exists(
        cls, name: str, room_type: str, project: int = PROJECT_ID
    ) -> bool:
        if not Hub.is_valid_room_name(name):
            raise AppException({"error": "Invalid room name"})

        if not Hub.room_exists(name):
            room_class = RoomType.get_class(room_type)
            cls.rooms[name] = room_class(name, project)
            logger.debug(f"Room created: {name}")
            return True

        return False

    @classmethod
    def is_valid_room_name(cls, name: str) -> bool:
        if len(name) == (cls.ROOM_NAME_LENGTH + 1) and name[0] == "d":
            return True

        if len(name) == cls.ROOM_NAME_LENGTH and name.isdigit():
            return True

        return False

    @classmethod
    def create_dev_room(
        cls, name: str, room_type: str, project: int = PROJECT_ID
    ) -> str:
        if len(name) != 4 or name[0] != "d":
            msg = "Wrong dev room name (lenght must be 4, first letter must be 'd')"
            raise AppException({"error": msg})

        if name not in cls.rooms:
            room_class = RoomType.get_class(room_type)
            cls.rooms[name] = room_class(name, project)

            logger.debug(f"Room created: {name}")

        return name

    @classmethod
    async def add_client(cls, room_name: str, websocket: WebSocket) -> str:
        if room_name not in cls.rooms:
            raise RoomDoesNotExist()

        room = cls.rooms[room_name]
        client_key = room.add_client(websocket)
        logger.debug(f"Client joined room: {room_name}, {client_key}")

        msg = {"event": "key_assigned", "key": client_key, "room_size": room.size}
        await room.direct_send("server", client_key, msg)
        msg = {"event": "client_added", "key": client_key}
        await room.send("server", msg)

        return client_key

    @classmethod
    async def add_spectator(cls, room_name: str, websocket: WebSocket) -> str:
        if room_name not in cls.rooms:
            raise RoomDoesNotExist()

        room = cls.rooms[room_name]
        client_key = room.add_spectator(websocket)
        logger.debug(f"Spectator joined room: {room_name}, {client_key}")

        return client_key

    @classmethod
    async def remove_client(cls, room_name: str, websocket: WebSocket) -> str | None:
        if room_name not in cls.rooms:
            raise RoomDoesNotExist()

        room = cls.rooms[room_name]
        client_key = room.remove_client(websocket)
        logger.debug(f"Client left room: {room_name}, {client_key}")

        if client_key:
            if room.is_empty and not room.is_dev:
                await cls.delete_room(room_name)
            else:
                msg = {"event": "client_removed", "key": client_key}
                await room.send("server", msg)

        return client_key

    @classmethod
    async def delete_room(cls, room_name: str) -> str | None:
        if not cls.rooms[room_name].is_empty:
            msg = {"event": "room_deleted"}
            await cls.rooms[room_name].send("server", msg)
            await cls.rooms[room_name].close_websocets()

        # This should not happen because closing last websocket delete room
        if room_name in cls.rooms:
            del cls.rooms[room_name]
            logger.debug(f"Room deleted: {room_name}")

        return None

    @classmethod
    async def remove_spectator(cls, room_name: str, websocket: WebSocket) -> str | None:
        if room_name not in cls.rooms:
            raise RoomDoesNotExist()

        room = cls.rooms[room_name]
        client_key = room.remove_spectator(websocket)

        return client_key

    @classmethod
    async def send(cls, room_name: str, sender: WebSocket, data: dict):
        if room_name not in cls.rooms:
            raise RoomDoesNotExist()

        room = cls.rooms[room_name]
        sender_key = room.get_client_key(sender)
        if sender_key:
            await room.send(sender_key, data)
