import logging
import htpy as html

from starlette import status
from starlette.endpoints import HTTPEndpoint, WebSocketEndpoint
from starlette.requests import Request
from starlette.responses import PlainTextResponse, HTMLResponse, JSONResponse
from starlette.websockets import WebSocket
from starlette.exceptions import WebSocketException, HTTPException
from starlette.authentication import requires

from core.exceptions import AppException
from hub.services import Hub, RoomType
from core.html import page, nav
from core.endpoints import ApiEndpoint
from hub.html import rooms_table
from events.services import InternalEvent, EventKind
from app.settings import PROJECT_ID

logger = logging.getLogger(__name__)


class ProjectExistsMixin:
    def _projectExists(id: int):
        return True


class CreateRoomApiEndpoint(ProjectExistsMixin, ApiEndpoint):
    async def get(self, request: Request):
        room_type = request.query_params.get("type", RoomType.PLAIN)
        if "dev" in request.query_params:
            name = request.query_params.get("dev", "d001")
            Hub.create_dev_room(name, room_type)
        else:
            name = Hub.create_room(room_type)

        await InternalEvent.capture(
            msg="create room",
            kind=EventKind.ANALYTICS,
            location="/hub/endpoints/CreateRoomApiEndpoint",
            # data={"user": request.user.id, "room": name},
            data={"room": name, "project": PROJECT_ID},
            tags=["api"],
            request=request,
        )

        return PlainTextResponse(name)

    async def post(self, request: Request):
        data = await self._get_json_body(request)
        data = await self._validate(data)
        room_type = data["type"]
        project = data["project"]

        if "dev" in data:
            name = data.get("dev")
            Hub.create_dev_room(name, room_type, project)
        else:
            name = Hub.create_room(room_type, project)

        await InternalEvent.capture(
            msg="create room",
            kind=EventKind.ANALYTICS,
            location="/hub/endpoints/CreateRoomApiEndpoint",
            # data={"user": request.user.id, "room": name},
            data={"room": name, "project": project},
            tags=["api"],
            request=request,
        )

        return JSONResponse(content={"name": name})

    async def _validate(self, data: dict):
        room_type = data.get("type")
        if room_type not in RoomType.all():
            data["type"] = RoomType.PLAIN

        try:
            project = int(data.get("project", PROJECT_ID))
        except ValueError:
            raise AppException({"error": "Invalid project"})

        if project != PROJECT_ID and not self._projectExists(project):
            raise AppException({"error": "Project do not exists"})

        data["project"] = project

        return data


class CreateRoomHxEndpoint(ProjectExistsMixin, HTTPEndpoint):
    def _html(self, data: dict) -> html.Element:
        return html.render_node(
            [
                rooms_table(data),
                html.div("#form-msg.error", {"hx-swap-oob": "true"})[data["msg"]],
            ]
        )

    @requires(["authenticated", "admin"], redirect="login")
    async def post(self, request: Request):
        room = {}
        async with request.form() as form:
            room["name"] = form.get("name")
            room["type"] = form.get("type") or RoomType.PLAIN
            room["project"] = form.get("project") or PROJECT_ID

        msg = ""
        if room["name"]:
            try:
                Hub.create_room_if_not_exists(
                    room["name"], room["type"], room["project"]
                )
            except AppException as e:
                msg = e.data.get("error", "Unknown error")
        else:
            Hub.create_room(room["type"])

        data = {"rooms": Hub.rooms, "msg": msg}

        await InternalEvent.capture(
            msg="create room",
            kind=EventKind.ANALYTICS,
            location="/hub/endpoints/CreateRoomHxEndpoint",
            data={"user": request.user.id, "room": room["name"]},
            tags=["web"],
            request=request,
        )

        return HTMLResponse(self._html(data))


class DeleteRoomHxEndpoint(HTTPEndpoint):
    @requires(["authenticated", "admin"], redirect="login")
    async def get(self, request: Request):
        room = request.path_params["room"]
        await Hub.delete_room(room)
        data = {"rooms": Hub.rooms}

        return HTMLResponse(rooms_table(data))


class JoinRoomWebSocket(WebSocketEndpoint):
    encoding = "text"

    async def on_connect(self, websocket: WebSocket):
        room = websocket.path_params["room"]
        room_type = websocket.query_params.get("type", RoomType.PLAIN)
        Hub.create_room_if_not_exists(room, room_type)

        await websocket.accept()
        await Hub.add_client(room, websocket)

    async def on_receive(self, websocket: WebSocket, data: str):
        room = websocket.path_params["room"]
        await Hub.send(room_name=room, sender=websocket, data=data)

    async def on_disconnect(self, websocket: WebSocket, close_code: int):
        room = websocket.path_params["room"]
        await Hub.remove_client(room, websocket)


class WatchRoomWebSocket(WebSocketEndpoint):
    encoding = "text"

    @requires(["authenticated", "admin"], redirect="login")
    async def on_connect(self, websocket: WebSocket):
        room = websocket.path_params["room"]
        if not Hub.room_exists(room):
            raise WebSocketException(
                code=status.WS_1008_POLICY_VIOLATION, reason="Room does not exists"
            )

        await websocket.accept()
        await Hub.add_spectator(room, websocket)

    async def on_receive(self, websocket: WebSocket, data: str):
        pass

    async def on_disconnect(self, websocket: WebSocket, close_code: int):
        room = websocket.path_params["room"]
        await Hub.remove_spectator(room, websocket)


class HubHomePageEndpoint(HTTPEndpoint):
    def _html(self, request: Request, data: dict) -> html.Element:
        body = [
            nav(request.user.admin),
            html.main(".container.f-col.f-ai-center")[
                html.h1["Hub"],
                html.div("#rooms")[rooms_table(data)],
                html.form[
                    html.fieldset(role="group")[
                        html.input(name="name", type="text", placeholder="Name"),
                        html.select(name="type", required="")[
                            html.option["plain"], html.option["master"]
                        ],
                        html.input(
                            {"hx-post": "hx-create", "hx-target": "#rooms"},
                            type="submit",
                            value="Create",
                        ),
                    ]
                ],
                html.div("#form-msg"),
            ],
        ]
        return page(title="Hub", body=body)

    @requires(["authenticated", "admin"], redirect="login")
    async def get(self, request: Request):
        data = {"rooms": Hub.rooms}

        return HTMLResponse(self._html(request, data))


class RoomPageEndpoint(HTTPEndpoint):
    def _js(self, data: dict) -> str:
        ws = "{}hub/watch/{}".format(data["base_url"], data["room"].name)
        return """
            const socket = new WebSocket("{}");

            socket.addEventListener("message", (event) => {{
                const data = JSON.parse(event.data);
                let tr = document.createElement("tr");
                let td_sender = document.createElement("td");
                let td_data = document.createElement("td");
                td_sender.textContent = data["sender"];
                td_data.textContent = data["data"];
                if (data["sender"] === "server") {{
                    td_data.textContent = JSON.stringify(data["data"]);
                }}
                tr.append(td_sender);
                tr.append(td_data);
                document.getElementById("messages").prepend(tr);
            }});
        """.format(ws)

    def _html(self, request: Request, data: dict) -> html.Element:
        room = data["room"]
        body = [
            nav(request.user.admin),
            html.main(".f-col.f-ai-center")[
                html.h1[room.name],
                html.table(".striped")[
                    html.thead[
                        html.tr[html.th(style="width: 20%;")["Sender"], html.th["Data"]]
                    ],
                    html.tbody("#messages"),
                ],
            ],
            html.script[self._js(data)],
        ]
        return page(title="Hub", body=body)

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        room = request.path_params["room"]
        if room not in Hub.rooms:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
        data = {"room": Hub.rooms[room], "base_url": request.base_url}

        return HTMLResponse(self._html(request, data))
