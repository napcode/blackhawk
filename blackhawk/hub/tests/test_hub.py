from unittest import TestCase
from starlette import status
from starlette.testclient import TestClient

from hub.services import Hub, Room, MasterRoom, RoomType
from app.app import app
from core.tests.common import ComplexTestCase


class TestCreateRoomApiEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        Hub.clear()
        self.client = TestClient(app)

    def test_get_create_unknown(self):
        response = self.client.get("/hub/create")
        assert response.status_code == status.HTTP_200_OK

        room_name = response.text
        assert Hub.room_exists(room_name)
        assert isinstance(Hub.rooms[room_name], Room)

    def test_get_create_broadcast(self):
        response = self.client.get("/hub/create?type=plain")
        assert response.status_code == status.HTTP_200_OK

        room_name = response.text
        assert Hub.room_exists(room_name)
        assert isinstance(Hub.rooms[room_name], Room)

    def test_get_create_master(self):
        response = self.client.get("/hub/create?type=master")
        assert response.status_code == status.HTTP_200_OK

        room_name = response.text
        assert Hub.room_exists(room_name)
        assert isinstance(Hub.rooms[room_name], MasterRoom)

    def test_get_create_master_dev(self):
        response = self.client.get("/hub/create?type=master&dev=d001")
        assert response.status_code == status.HTTP_200_OK

        room_name = response.text
        assert Hub.room_exists(room_name)
        assert isinstance(Hub.rooms[room_name], MasterRoom)

    def test_get_create_master_dev_wrong_name(self):
        response = self.client.get("/hub/create?type=master&dev=100")
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    def test_post_create_unknown(self):
        response = self.client.post("/hub/create", json={})
        assert response.status_code == status.HTTP_200_OK

        data = response.json()
        assert "name" in data
        name = data["name"]
        assert Hub.room_exists(name)
        assert isinstance(Hub.rooms[name], Room)

    def test_post_create_broadcast(self):
        data = {"type": "plain"}
        response = self.client.post("/hub/create", json=data)
        assert response.status_code == status.HTTP_200_OK

        data = response.json()
        assert "name" in data
        name = data["name"]
        assert Hub.room_exists(name)
        assert isinstance(Hub.rooms[name], Room)

    def test_post_create_master(self):
        data = {"type": "master"}
        response = self.client.post("/hub/create", json=data)
        assert response.status_code == status.HTTP_200_OK

        data = response.json()
        assert "name" in data
        name = data["name"]
        assert Hub.room_exists(name)
        assert isinstance(Hub.rooms[name], Room)

    def test_post_create_master_dev(self):
        data = {"type": "master", "dev": "d100"}
        response = self.client.post("/hub/create", json=data)
        assert response.status_code == status.HTTP_200_OK

        data = response.json()
        assert "name" in data
        name = data["name"]
        assert Hub.room_exists(name)
        assert isinstance(Hub.rooms[name], Room)

    def test_post_create_master_dev_wrong_name(self):
        data = {"type": "master", "dev": "100"}
        response = self.client.post("/hub/create", json=data)
        assert response.status_code == status.HTTP_400_BAD_REQUEST


class TestJoinRoomWebSocket(TestCase):
    def setUp(self):
        Hub.clear()
        self.client = TestClient(app)

    def test_broadcast_send(self):
        msg = "test"
        name = Hub.create_room(RoomType.PLAIN)
        client = TestClient(app)
        with client.websocket_connect(f"/hub/join/{name}") as websocket1:
            message = websocket1.receive_json()  # Wait for server events
            assert message["sender"] == "server"
            message = websocket1.receive_json()  # Wait for server events
            assert message["sender"] == "server"
            with client.websocket_connect(f"/hub/join/{name}") as websocket2:
                message = websocket1.receive_json()  # Wait for server events
                assert message["sender"] == "server"
                message = websocket2.receive_json()  # Wait for server events
                assert message["sender"] == "server"
                message = websocket2.receive_json()  # Wait for server events
                assert message["sender"] == "server"
                websocket1.send_text(msg)
                message = websocket1.receive_json()
                assert message["data"] == msg
                message = websocket2.receive_json()
                assert message["data"] == msg

    def test_master_send(self):
        msg = "test"
        msg2 = "test2"
        name = Hub.create_room(RoomType.MASTER)
        client = TestClient(app)
        with client.websocket_connect(f"/hub/join/{name}") as websocket1:
            message = websocket1.receive_json()  # Wait for server events
            assert message["sender"] == "server"
            message = websocket1.receive_json()  # Wait for server events
            assert message["sender"] == "server"
            with client.websocket_connect(f"/hub/join/{name}") as websocket2:
                message = websocket1.receive_json()  # Wait for server events
                assert message["sender"] == "server"
                message = websocket2.receive_json()  # Wait for server events
                assert message["sender"] == "server"
                websocket1.send_text(msg)
                message = websocket2.receive_json()
                assert message["data"] == msg
                websocket2.send_text(msg2)
                message = websocket1.receive_json()
                assert message["data"] == msg2


# class TestWatchRoomWebSocket(TestCase):
#     def setUp(self):
#         Hub.clear()
#         self.client = TestClient(app)

#     def test_broadcast_send(self):
#         msg = "test"
#         name = Hub.create_room(RoomType.PLAIN)
#         client = TestClient(app)
#         with client.websocket_connect(f"/hub/join/{name}") as websocket1:
#             message = websocket1.receive_json()  # Wait for server events
#             with client.websocket_connect(f"/hub/watch/{name}") as websocket2:
#                 websocket1.send_text(msg)
#                 message = websocket1.receive_json()
#                 assert message["data"] == msg
#                 message = websocket2.receive_json()
#                 assert message["data"] == msg

#     def test_master_send(self):
#         msg = "test"
#         name = Hub.create_room(RoomType.MASTER)
#         client = TestClient(app)
#         with client.websocket_connect(f"/hub/join/{name}") as websocket1:
#             message = websocket1.receive_json()  # Wait for server events
#             with client.websocket_connect(f"/hub/watch/{name}") as websocket2:
#                 websocket1.send_text(msg)
#                 message = websocket2.receive_json()
#                 assert message["data"] == msg


class TestCreateRoomHxEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        Hub.clear()

    def tearDown(self):
        pass

    def test_create_broadcast(self):
        response = self.client.post("/hub/hx-create", data={"type": "plain"})
        assert response.status_code == status.HTTP_200_OK
        assert len(Hub.rooms) == 1
        assert isinstance(list(Hub.rooms.values())[0], Room)

    def test_create_master(self):
        name = "100"
        response = self.client.post(
            "/hub/hx-create", data={"type": "master", "name": name}
        )
        assert response.status_code == status.HTTP_200_OK
        assert len(Hub.rooms) == 1
        room = list(Hub.rooms.values())[0]
        assert isinstance(room, Room)
        assert room.name == name

    def test_create_dev(self):
        name = "d100"
        response = self.client.post("/hub/hx-create", data={"name": name})
        assert response.status_code == status.HTTP_200_OK
        assert len(Hub.rooms) == 1
        room = list(Hub.rooms.values())[0]
        assert isinstance(room, Room)
        assert room.name == name


class TestDeleteRoomHxEndpoint(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        Hub.clear()

    def tearDown(self):
        pass

    def test_delete(self):
        room = Hub.create_room(RoomType.PLAIN)
        response = self.client.get(f"/hub/hx-delete/{room}")
        assert response.status_code == status.HTTP_200_OK

        assert len(Hub.rooms) == 0


class TestHubPages(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        self.user = self.create_user()
        self.token = self.create_token(self.user)
        self.client = self.create_authorized_client(self.token)
        Hub.clear()

    def tearDown(self):
        pass

    def test_hub_page(self):
        response = self.client.get("/hub")
        assert response.status_code == status.HTTP_200_OK

    def test_room_page(self):
        name = Hub.create_room(RoomType.PLAIN)
        response = self.client.get(f"/hub/room/{name}")
        assert response.status_code == status.HTTP_200_OK
