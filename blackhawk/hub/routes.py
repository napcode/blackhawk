from starlette.routing import Route, WebSocketRoute

from hub import endpoints

routes = [
    Route("/", endpoints.HubHomePageEndpoint),
    Route("/create", endpoints.CreateRoomApiEndpoint),
    Route("/hx-create", endpoints.CreateRoomHxEndpoint),
    Route("/hx-delete/{room}", endpoints.DeleteRoomHxEndpoint),
    Route("/room/{room}", endpoints.RoomPageEndpoint),
    WebSocketRoute("/join/{room}", endpoints.JoinRoomWebSocket),
    WebSocketRoute("/watch/{room}", endpoints.WatchRoomWebSocket),
]
