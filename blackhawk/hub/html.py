import htpy as html

from hub.services import Room


def room_row(room: Room) -> html.Element:
    return html.tr[
        html.td[room.name],
        html.td[room.size],
        html.td(".f-row.f-jc-space-evenly")[
            html.a(href=f"room/{room.name}")["watch"],
            html.a(
                {"hx-get": f"hx-delete/{room.name}", "hx-target": "#rooms"}, href="#"
            )["delete"],
        ],
    ]


def rooms_table(data: dict) -> html.Element:
    return html.table(".striped", style="width: 400px")[
        html.thead[
            html.tr[
                html.th["name"],
                html.th["clients"],
                html.th["actions"],
            ]
        ],
        *[room_row(r) for r in data["rooms"].values()],
    ]
