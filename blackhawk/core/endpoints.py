from starlette.endpoints import HTTPEndpoint
from starlette.responses import HTMLResponse, PlainTextResponse, RedirectResponse
from starlette.authentication import requires
from starlette.requests import Request

import htpy as html

from core.html import page, nav
from core.services import login, logout
from core.exceptions import AppException
from core.tables import Project

from events.services import InternalEvent, EventKind


class HomeEndpoint(HTTPEndpoint):
    def _html(self, request: Request) -> html.Element:
        return page(
            title="home",
            body=[
                nav(request.user.admin),
                html.main(".f-col.f-ai-center")[html.h1["Dashboard"]],
            ],
        )

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        return HTMLResponse(self._html(request))


class LoginEndpoint(HTTPEndpoint):
    def _html(self) -> html.Element:
        body = [
            html.main(".f-col.f-ai-center.f-jc-center")[
                html.h1["Blackhawk"],
                html.form({"hx-post": "", "hx-target": "#form-msg"})[
                    html.fieldset[
                        html.label[
                            "Username",
                            html.input(name="username", placeholder="username"),
                        ],
                        html.label[
                            "Password",
                            html.input(
                                name="password",
                                type="password",
                                placeholder="password",
                            ),
                        ],
                        html.div("#form-msg"),
                    ],
                    html.input(type="submit", value="Log in"),
                ],
            ]
        ]
        return page(title="login", body=body)

    async def get(self, request: Request):
        return HTMLResponse(self._html())

    def login_error_msg(self) -> html.Element:
        return html.span(".error")["Incorrect username or password"]

    async def post(self, request: Request):
        form = await request.form()
        username = str(form.get("username", ""))
        password = str(form.get("password", ""))
        user, token = await login(username, password)
        if not token:
            return HTMLResponse(self.login_error_msg())

        response = PlainTextResponse("", headers={"HX-Redirect": "/"})
        response.set_cookie(
            "Authorization",
            f"Token {token}",
            samesite="strict",
            httponly=True,
            secure=True,
            max_age=10 * 3600,
        )
        await InternalEvent.capture(
            msg="login",
            kind=EventKind.ANALYTICS,
            location="/core/endpoints/LoginEndpoint",
            data={"id": user, "username": form.get("username")},
            tags=["web"],
            request=request,
        )

        return response


class LogoutEndpoint(HTTPEndpoint):
    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        await logout(request.user)
        response = RedirectResponse(url="/core/login")
        response.delete_cookie("Authorization", httponly=True, secure=True)

        await InternalEvent.capture(
            msg="logout",
            kind=EventKind.ANALYTICS,
            location="/core/endpoints/LogoutEndpoint",
            data={"id": request.user.id, "username": request.user.username},
            tags=["web"],
            request=request,
        )

        return response


class ApiEndpoint(HTTPEndpoint):
    async def _get_json_body(self, request):
        try:
            data = await request.json()
        except:
            raise AppException({"error": "Invalid json body"})

        return data


class ProjectsPageEndpoint(HTTPEndpoint):
    def _project_row(self, project: dict) -> html.Element:
        return html.tr[
            html.td[html.small[str(project["key"])]],
            html.td[project["name"]],
            html.td[project["owner.username"]],
            html.td(".f-row.f-jc-space-evenly")[
                "---"
                #     html.a(href=f"edit/{project['key']}")["edit"],
            ],
        ]

    def _html(self, request: Request, data: dict) -> html.Element:
        body = [
            nav(request.user.admin),
            html.main(".container.f-col.f-ai-center.f-jc-center")[
                html.h1["Projects"],
                html.table(".striped")[
                    html.thead[
                        html.tr()[
                            html.th["key"],
                            html.th["name"],
                            html.th["owner"],
                            html.th["actions"],
                        ]
                    ],
                    [self._project_row(p) for p in data["projects"]],
                ],
            ],
        ]

        return page(title="projects", body=body)

    @requires(["authenticated"], redirect="login")
    async def get(self, request: Request):
        projects = await Project.select(
            Project.key, Project.name, Project.owner.username
        )
        data = {"projects": projects}

        return HTMLResponse(self._html(request, data))
