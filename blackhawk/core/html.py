import htpy as html


def page(title: str = "", body: html.Node = None) -> html.Element:
    return html.html(lang="en")[
        html.head[
            html.meta(charset="utf-8"),
            html.meta(content="width=device-width, initial-scale=1", name="viewport"),
            html.link(href="/static/pico.min.css", rel="stylesheet"),
            html.link(href="/static/main.css", rel="stylesheet"),
            html.link(href="/static/helpers.css", rel="stylesheet"),
            html.script(src="/static/htmx.min.js"),
            html.title[f"Blackhawk - {title}"],
        ],
        body,
    ]


def nav(admin: bool) -> html.Element:
    return html.nav(".container")[
        html.ul[
            html.li[html.strong[html.a(href="/")["Blackhawk"]]],
            html.li[html.a(href="/core/projects/")["projects"]],
            html.li[html.a(href="/events/")["events"]],
            html.li[html.a(href="/storage/")["storage"]],
            admin and html.li[html.a(href="/hub/")["hub"]],
        ],
        html.ul[
            html.li[
                html.a(href="/core/logout")[html.button(".outline.secondary")["logout"]]
            ]
        ],
    ]
