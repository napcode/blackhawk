class AppException(Exception):
    data: dict

    def __init__(self, data: dict):
        self.data = data or {}
