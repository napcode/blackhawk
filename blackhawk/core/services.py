from typing import Tuple

from piccolo.apps.user.tables import BaseUser
from core.tables import AuthToken
from core.authentication import AuthUser

WEB_AUTH_TOKEN_NAME = "web-auth"


async def login(username: str, password: str) -> Tuple[int | None, str | None]:
    user = await BaseUser.login(username, password)
    if not user:
        return None, None

    token = (
        await AuthToken.select(AuthToken.value)
        .where(AuthToken.user == user)
        .where(AuthToken.name == WEB_AUTH_TOKEN_NAME)
        .first()
    )

    if not token:
        token = AuthToken(name=WEB_AUTH_TOKEN_NAME, user=user).generate_value()
        await token.save()
        token_value = token.value
    else:
        token_value = token["value"]

    return user, token_value


async def logout(user: AuthUser) -> None:
    if user.is_authenticated:
        await (
            AuthToken.delete()
            .where(AuthToken.value == user.token)
            .where(AuthToken.user == user.id)
        )
