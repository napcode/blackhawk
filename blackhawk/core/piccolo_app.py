"""
Import all of the Tables subclasses in your app here, and register them with
the APP_CONFIG.
"""

import os

from piccolo.conf.apps import AppConfig, table_finder
from core.commands.run_server import run_server
from core.commands.collect_static import collect_static
from core.commands.version import version


CURRENT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))


APP_CONFIG = AppConfig(
    app_name="core",
    migrations_folder_path=os.path.join(CURRENT_DIRECTORY, "piccolo_migrations"),
    table_classes=table_finder(modules=["core.tables"], exclude_imported=True),
    migration_dependencies=[],
    commands=[run_server, collect_static, version],
)
