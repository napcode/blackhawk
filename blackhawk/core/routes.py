from starlette.routing import Route

from core import endpoints

routes = [
    Route("/", endpoints.HomeEndpoint),
    Route("/login", endpoints.LoginEndpoint, name="login"),
    Route("/logout", endpoints.LogoutEndpoint, name="logout"),
    Route("/projects", endpoints.ProjectsPageEndpoint, name="projects"),
]
