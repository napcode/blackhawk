import os
import shutil

from importlib import resources
from app import static, settings
from events.services import InternalEvent, EventKind


def create_static_dir(static_dir: str):
    if os.path.exists(static_dir):
        shutil.rmtree(static_dir)

    os.makedirs(static_dir)


def create_static_files(static_dir: str):
    static_resource = resources.files(static)
    for path in static_resource.iterdir():
        with path.open("rb") as sf:  # Source file
            content = sf.read()
            file_name = path.parts[-1]
            with open(f"{static_dir}/{file_name}", "wb") as df:  # Destination file
                df.write(content)


async def collect_static(static_dir: str = settings.STATIC_DIR):
    """
    Collect static files.
    """
    await InternalEvent.capture(
        msg="collect_static",
        kind=EventKind.ANALYTICS,
        location="/core/commands",
        tags=["cmd"],
    )
    create_static_dir(static_dir)
    create_static_files(static_dir)
