from app import settings
import asyncio

from events.services import InternalEvent, EventKind


def run_server(host: str = "localhost", port: int = 8000):
    """
    Run web server.
    """
    asyncio.run(
        InternalEvent.capture(
            msg="run_server",
            kind=EventKind.ANALYTICS,
            location="/core/commands",
            tags=["cmd"],
        )
    )
    import uvicorn

    uvicorn.run("app.app:app", reload=settings.DEBUG, host=host, port=port)
