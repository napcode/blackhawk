from app import piccolo_conf


def version():
    """
    Prints app version
    """
    print(piccolo_conf.VERSION)
