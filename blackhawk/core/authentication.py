from dataclasses import dataclass

from starlette.authentication import (
    AuthCredentials,
    AuthenticationBackend,
    AuthenticationError,
    BaseUser,
    UnauthenticatedUser,
)

from .tables import AuthToken


@dataclass
class AuthUser(BaseUser):
    id: int
    token: str
    username: str = ""
    admin: bool = False
    superuser: bool = False

    @property
    def is_authenticated(self) -> bool:
        return True

    @property
    def display_name(self) -> str:
        return self.username


class TokenAuthBackend(AuthenticationBackend):
    async def authenticate(self, conn):
        auth_header = conn.headers.get("Authorization")
        auth_cookie = conn.cookies.get("Authorization")
        if not auth_header and not auth_cookie:
            return

        auth = auth_header or auth_cookie
        if not auth.startswith("Token "):
            raise AuthenticationError("Invalid auth credentials")

        token = auth.split(" ")[1]
        user_data = (
            await AuthToken.select(
                AuthToken.user._.id.as_alias("id"),
                AuthToken.user._.username.as_alias("username"),
                AuthToken.user._.admin.as_alias("admin"),
                AuthToken.user._.superuser.as_alias("superuser"),
            )
            .where(AuthToken.value == token)
            .where(AuthToken.user._.active == True)
            .first()
        )
        user = UnauthenticatedUser
        credentials = []
        if user_data:
            credentials.append("authenticated")
            user = AuthUser(**user_data, token=token)
            if user.admin:
                credentials.append("admin")
            if user.superuser:
                credentials.append("superuser")

        return AuthCredentials(credentials), user
