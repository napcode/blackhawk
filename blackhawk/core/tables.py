from secrets import token_urlsafe

from piccolo.table import Table
from piccolo.columns import Text, ForeignKey, UUID
from piccolo.apps.user.tables import BaseUser


class Project(Table):
    name = Text()
    key = UUID(unique=True)
    owner = ForeignKey(references=BaseUser)


class AuthToken(Table):
    name = Text()
    value = Text()
    user = ForeignKey(references=BaseUser)

    def generate_value(self):
        self.value = token_urlsafe(32)

        return self
