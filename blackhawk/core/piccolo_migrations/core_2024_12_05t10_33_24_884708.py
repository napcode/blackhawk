from piccolo.apps.migrations.auto.migration_manager import MigrationManager
from core.tables import Project


ID = "2024-12-05T10:33:24:884708"
VERSION = "1.22.0"
DESCRIPTION = "Add project entry for blackhawk"


async def forwards():
    manager = MigrationManager(migration_id=ID, app_name="", description=DESCRIPTION)

    async def run():
        await Project(id=1, name="Blackhawk").save()
        await Project.raw("ALTER SEQUENCE project_id_seq RESTART WITH 2")

    manager.add_raw(run)

    return manager
