import uuid
from unittest import TestCase
from starlette.testclient import TestClient

from piccolo.table import create_db_tables_sync, drop_db_tables_sync
from piccolo.apps.migrations.commands.forwards import run_forwards
from piccolo.testing.model_builder import ModelBuilder
from piccolo.apps.user.tables import BaseUser
from piccolo.conf.apps import Finder

from core.services import WEB_AUTH_TOKEN_NAME
from core.tables import AuthToken, Project

from app.app import app

TABLES = Finder().get_table_classes()


class ComplexTestCase(TestCase):
    def create_db_tables(self):
        self.drop_db_tables()
        create_db_tables_sync(*TABLES)
        Project(id=1, name="Blackhawk", key=str(uuid.uuid4())).save().run_sync()
        Project.raw("ALTER SEQUENCE project_id_seq RESTART WITH 2").run_sync()

    def create_user(self) -> BaseUser:
        return ModelBuilder.build_sync(
            BaseUser,
            defaults={
                "active": True,
                "admin": True,
                "username": "user",
            },
        )

    def create_token(self, user: BaseUser) -> AuthToken:
        token = AuthToken(name=WEB_AUTH_TOKEN_NAME, value="123", user=user)
        token.save().run_sync()
        return token

    def create_project(self, user: BaseUser, name: str = "Project") -> Project:
        project = Project(name=name, key=str(uuid.uuid4()), owner=user)
        project.save().run_sync()
        return project

    def create_authorized_client(self, token: AuthToken) -> TestClient:
        client = TestClient(app)
        client.headers = {"Authorization": f"Token {token.value}"}
        return client

    def create_unauthorized_client(self) -> TestClient:
        client = TestClient(app, follow_redirects=False)
        return client

    def drop_db_tables(self):
        drop_db_tables_sync(*TABLES)
