from starlette import status

from piccolo.apps.user.tables import BaseUser
from piccolo.utils.sync import run_sync

from core.tables import AuthToken
from core.services import login, WEB_AUTH_TOKEN_NAME, logout
from core.tests.common import ComplexTestCase
from core.authentication import AuthUser


class TestLogin(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        user = self.create_user()
        BaseUser.update_password_sync(user=user.username, password="pass123")
        self.user = BaseUser.select().first().run_sync()
        self.token = self.create_token(user)
        self.client = self.create_unauthorized_client()

    def test_login_service_no_user(self):
        user, token = run_sync(login("wrong user", "pass123"))
        assert user is None
        assert token is None

    def test_login_service_existing_token(self):
        user, token = run_sync(login(self.user["username"], "pass123"))
        assert user == self.user["id"]
        assert token == self.token.value

    def test_login_service_new_token(self):
        AuthToken.delete().where(AuthToken.name == WEB_AUTH_TOKEN_NAME).run_sync()
        user, token = run_sync(login(self.user["username"], "pass123"))
        assert user == self.user["id"]
        assert len(token) == 43

    def test_login_endpoint_correct(self):
        response = self.client.post(
            "/core/login",
            data={"username": self.user["username"], "password": "pass123"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert "Hx-Redirect" in response.headers
        assert "Authorization" in response.cookies

    def test_login_endpoint_incorrect(self):
        response = self.client.post(
            "/core/login",
            data={"username": self.user["username"], "password": "bad pass"},
        )
        assert response.status_code == status.HTTP_200_OK
        assert "Hx-Redirect" not in response.headers


class TestLogout(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        user = self.create_user()
        BaseUser.update_password_sync(user=user.username, password="pass123")
        self.user = BaseUser.select().first().run_sync()
        self.token = self.create_token(user)
        self.client = self.create_authorized_client(self.token)

    def test_logout_service(self):
        user = AuthUser(id=self.user["id"], token=self.token.value)
        run_sync(logout(user))

        count = AuthToken().count().run_sync()
        assert count == 0

    def test_logout_endpoint_correct(self):
        response = self.client.get("/core/logout")
        assert response.status_code == status.HTTP_200_OK
        assert "Authorization" not in response.cookies

        count = AuthToken().count().run_sync()
        assert count == 0


class TestCorePages(ComplexTestCase):
    def setUp(self):
        self.create_db_tables()
        user = self.create_user()
        BaseUser.update_password_sync(user=user.username, password="pass123")
        self.user = BaseUser.select().first().run_sync()
        self.token = self.create_token(user)
        self.client = self.create_authorized_client(self.token)
        self.project = self.create_project(user)

    def test_home_page(self):
        response = self.client.get("/")
        assert response.status_code == status.HTTP_200_OK

    def test_projects_page(self):
        response = self.client.get("/core/projects")
        assert response.status_code == status.HTTP_200_OK
