#! /bin/bash

version=$1

echo "pex -v blackhawk/ -r requirements.txt --disable-cache -c blackhawk -o build/blackhawk-${1:-dev}.pex"
pex -v blackhawk/ -r requirements.txt --disable-cache -c blackhawk -o build/blackhawk-${1:-dev}.pex

